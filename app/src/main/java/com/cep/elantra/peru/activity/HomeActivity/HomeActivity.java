package com.cep.elantra.peru.activity.HomeActivity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.cep.elantra.peru.R;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }
}
