package com.cep.elantra.peru.fragment.ReunionesFragment.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.cep.elantra.peru.R;
import com.cep.elantra.peru.retrofit.entitys.NosotrosEntity;
import com.cep.elantra.peru.retrofit.entitys.ReunionEntity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ReunionesAdapter extends RecyclerView.Adapter<ReunionesAdapter.ViewHolder> {

    public interface RecyclerListener {
        public void onObjectSelected(int position, ReunionEntity object);
    }

    private ReunionesAdapter.RecyclerListener mCallback;

    private ArrayList<ReunionEntity> mObjects;


    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.carView_transaction)
        public CardView m_CardViewTransaction;

        @BindView(R.id.tv_title)
        public TextView m_tvTitle;

        @BindView(R.id.tv_subtitle)
        public TextView m_tvSubTitle;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    public ReunionesAdapter(ReunionesAdapter.RecyclerListener callback) {
        mObjects = new ArrayList<>();
        this.mCallback = callback;
    }

    public void add(ReunionEntity item) {
        mObjects.add(item);
        notifyItemInserted(mObjects.size() - 1);
    }

    public void add(int position, ReunionEntity item) {
        mObjects.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        mObjects.remove(position);
        notifyItemRemoved(position);
    }

    public void removeAll() {
        int total = mObjects.size();
        for (int i = 0; i < total; i++) {
            mObjects.remove(0);
        }
        notifyItemRangeRemoved(0, total);
    }

    @Override
    public ReunionesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.list_item_reuniones, parent, false);
        ReunionesAdapter.ViewHolder vh = new ReunionesAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ReunionesAdapter.ViewHolder holder, final int position) {

        ReunionEntity data = mObjects.get(position);

        holder.m_tvTitle.setText(data.getTitle());

        holder.m_tvSubTitle.setText(data.getSubTitle());

        holder.m_CardViewTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCallback != null) {
                    mCallback.onObjectSelected(position, mObjects.get(position));
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mObjects.size();
    }

    public void setCallback(ReunionesAdapter.RecyclerListener callback) {
        this.mCallback = callback;
    }
}
