package com.cep.elantra.peru.fragment.base;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.cep.elantra.peru.R;

import java.util.Objects;

public abstract class BaseFragment extends Fragment {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    public static final String PREFERENCE = "preference";
    public static final String NAVIGATE_ALERTAS_KEY = "elantra.peru.preferences.key";

    public void saveInPreferences(String key, String value) {
        SharedPreferences mSharedPreference = getActivity().getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor mEditor = mSharedPreference.edit();
        mEditor.putString(key, value);
        mEditor.apply();
    }

    public String getPreferences(String key) {
        SharedPreferences mySPref = getActivity().getSharedPreferences(PREFERENCE, 0);
        return mySPref.getString(key, "");
    }


    public void showToastMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    public void setMaxLenghtEditText(EditText editText, int maxLenght) {
        editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLenght)});
    }


}