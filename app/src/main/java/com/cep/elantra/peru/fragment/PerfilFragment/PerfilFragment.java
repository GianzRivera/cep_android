package com.cep.elantra.peru.fragment.PerfilFragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.cep.elantra.peru.R;
import com.cep.elantra.peru.activity.DetalleItemActivity.DetalleItemActivity;
import com.cep.elantra.peru.fragment.ReunionesFragment.adapter.ReunionesAdapter;
import com.cep.elantra.peru.fragment.base.BaseFragment;
import com.cep.elantra.peru.retrofit.entitys.ReunionEntity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PerfilFragment extends BaseFragment {

    private static final String TAG = com.cep.elantra.peru.fragment.NosotroFragment.NosotrosFragment.class.getSimpleName();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_perfil, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

    }

}
