package com.cep.elantra.peru.activity.SplashActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.cep.elantra.peru.R;
import com.cep.elantra.peru.activity.LoginActivity.LoginActivity;
import com.cep.elantra.peru.activity.base.BaseActivity;
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        AppCenter.start(getApplication(), "f9f81e27-e9a9-4026-8755-0d27b2d3ab3c",
                Analytics.class, Crashes.class);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                navigateToLoginActivity();
            }
        }, 2000);
    }


    private void navigateToLoginActivity(){
        Intent loginIntent = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(loginIntent);
        finish();
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }


}
