package com.cep.elantra.peru.activity.DetalleItemActivity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.cep.elantra.peru.R;
import com.cep.elantra.peru.activity.base.BaseActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetalleItemActivity extends BaseActivity {

    private static final String TAG =  DetalleItemActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_item);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.ivBack)
    protected void onClickBack(){
        super.onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}
