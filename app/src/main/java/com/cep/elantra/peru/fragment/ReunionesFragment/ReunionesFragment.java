package com.cep.elantra.peru.fragment.ReunionesFragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.cep.elantra.peru.R;
import com.cep.elantra.peru.activity.DetalleItemActivity.DetalleItemActivity;
import com.cep.elantra.peru.fragment.NosotroFragment.adapter.NosotrosListAdapter;
import com.cep.elantra.peru.fragment.ReunionesFragment.adapter.ReunionesAdapter;
import com.cep.elantra.peru.fragment.base.BaseFragment;
import com.cep.elantra.peru.retrofit.entitys.NosotrosEntity;
import com.cep.elantra.peru.retrofit.entitys.ReunionEntity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ReunionesFragment extends BaseFragment implements ReunionesAdapter.RecyclerListener, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = com.cep.elantra.peru.fragment.NosotroFragment.NosotrosFragment.class.getSimpleName();
    private ArrayList<ReunionEntity> listaReuniones;
    private ReunionesAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reuniones, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        m_rvRecycler.setLayoutManager(linearLayoutManager);

        mAdapter = new ReunionesAdapter(this);
        m_rvRecycler.setAdapter(mAdapter);

        loadItemsNosotrosDummy();
        swipe_refresh_layout.setOnRefreshListener(this);
    }

    private void loadItemsNosotrosDummy() {
        listaReuniones = new ArrayList<>();

        ReunionEntity reunionEntity = new ReunionEntity();
        reunionEntity.setTitle("Reunión noviembre");
        reunionEntity.setSubTitle("Estimados, se les invita a la proxima reunión que será el viernes 15 de noviembre a las 8:30 PM.");
        mAdapter.add(reunionEntity);


        ReunionEntity reunionEntity1 = new ReunionEntity();
        reunionEntity1.setTitle("Reunión octubre");
        reunionEntity1.setSubTitle("Estimados, se les invita a la proxima reunión que será el viernes 15 de noviembre a las 8:30 PM.");
        mAdapter.add(reunionEntity1);

        ReunionEntity reunionEntity2 = new ReunionEntity();
        reunionEntity2.setTitle("Reunión setiembre");
        reunionEntity2.setSubTitle("Estimados, se les invita a la proxima reunión que será el viernes 15 de noviembre a las 8:30 PM.");
        mAdapter.add(reunionEntity2);

        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onObjectSelected(int position, ReunionEntity object) {
        Log.e(TAG, "onObjectSelected: " + object.getTitle() != null ? object.getTitle() : "" );
        navigateToDetailActivity();
    }

    private void navigateToDetailActivity(){
        if ( getActivity() == null ){
            return;
        }
        Intent intentDetailActivity = new Intent(getActivity(), DetalleItemActivity.class);
        startActivity(intentDetailActivity);
        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    @Override
    public void onRefresh() {
        swipe_refresh_layout.setRefreshing(false);
    }


    @NonNull
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;

    @BindView(R.id.rvRecycler)
    RecyclerView m_rvRecycler;


}
