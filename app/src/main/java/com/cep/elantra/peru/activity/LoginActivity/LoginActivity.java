package com.cep.elantra.peru.activity.LoginActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cep.elantra.peru.MainActivity;
import com.cep.elantra.peru.R;
import com.cep.elantra.peru.activity.base.BaseActivity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();

    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        SimpleDateFormat format= new SimpleDateFormat();
        System.out.println(format.toLocalizedPattern());
        System.out.println(format.toPattern());

    }




    @OnClick(R.id.btn_iniciar_sesion)
    protected void onClickIniciarSession(){
        navigateToMainActivity();

        if ( m_etUsuario.getText().toString().isEmpty()){
            showToastMessage("Ingrese un usuario válido");
            return;
        }

        if ( m_etPassword.getText().toString().isEmpty()){
            showToastMessage("Ingrese una contraseña válida");
            return;
        }

    }

    private void navigateToMainActivity(){
        Intent intentMA = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intentMA);
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Presiona otra vez para salir", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(() -> doubleBackToExitPressedOnce=false, 2000);
    }


    @BindView(R.id.btn_iniciar_sesion)
    protected Button m_btnIniciarSesion;

    @BindView(R.id.tv_registrarme)
    protected TextView m_tvRegistrarme;

    @BindView(R.id.et_password)
    protected EditText m_etPassword;

    @BindView(R.id.et_usuario)
    protected EditText m_etUsuario;

}
