package com.cep.elantra.peru;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;

import com.cep.elantra.peru.activity.base.BaseActivity;
import com.cep.elantra.peru.fragment.NosotroFragment.NosotrosFragment;
import com.cep.elantra.peru.fragment.PerfilFragment.PerfilFragment;
import com.cep.elantra.peru.fragment.ReunionesFragment.ReunionesFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity
        implements FragmentManager.OnBackStackChangedListener,
        BottomNavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    private int mCurrentItemId;

    private static MainActivity instance;

    private Fragment fragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
//        instance = this;
        mBottomNavigationView.setOnNavigationItemSelectedListener(this);

        mBottomNavigationView.setSelectedItemId(R.id.navigation_home);

      //  mBottomNavigationView.setItemIconTintList(null);
        mBottomNavigationView.setItemTextColor(new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_checked},
                        new int[]{-android.R.attr.state_checked}
                },
                new int[]{
                        ContextCompat.getColor(this, R.color.blackregular),
                        Color.parseColor("#192232")
                }
        ));

        getSupportFragmentManager().addOnBackStackChangedListener(this);

        if (getIntent() != null && getIntent().getExtras() != null) {
            int menuId = getIntent().getExtras().getInt("Menu");
            if ( mBottomNavigationView != null){
                this.mBottomNavigationView.setSelectedItemId(menuId);
            }
        }
    }

    @Override
    public void onBackStackChanged() {
        ActionBar actionBar = getSupportActionBar();
        int i = getSupportFragmentManager().getBackStackEntryCount();
        if (actionBar != null) {
            if (i > 0) {
                actionBar.setDisplayHomeAsUpEnabled(true);
            } else {
                actionBar.setDisplayHomeAsUpEnabled(false);
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        //This method is called when the up button is pressed. Just the pop back stack.
        getSupportFragmentManager().popBackStack();
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.navigation_home:
                fragment = new NosotrosFragment();
                break;
            case R.id.navigation_operations:
                fragment = new ReunionesFragment();
                break;
            case R.id.navigation_accounts:
               // fragment = new AccountsFragment();
                break;
            case R.id.navigation_profile:
                fragment = new PerfilFragment();
               // fragment = new ProfileFragment();
                break;
        }

        if (fragment != null && mCurrentItemId != id) {
            mCurrentItemId = id;
            FragmentManager fm = getSupportFragmentManager();
            for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                fm.popBackStack();
            }
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, fragment)
                    .commit();

            return true;
        }

        return false;
    }



    @Override
    public void onBackPressed() {
        return;
    }

    @BindView(R.id.navigation)
    BottomNavigationView mBottomNavigationView;
}