package com.cep.elantra.peru.fragment.NosotroFragment.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.cep.elantra.peru.R;
import com.cep.elantra.peru.retrofit.entitys.NosotrosEntity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class NosotrosListAdapter extends RecyclerView.Adapter<NosotrosListAdapter.ViewHolder> {

    public interface RecyclerListener {
        public void onObjectSelected(int position, NosotrosEntity object);
    }

    private NosotrosListAdapter.RecyclerListener mCallback;

    private ArrayList<NosotrosEntity> mObjects;

    private SimpleDateFormat sdfInput, sdfOutput;

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.carView_transaction)
        public CardView m_CardViewTransaction;

        @BindView(R.id.tv_title)
        public TextView m_tvTitle;

        @BindView(R.id.tv_subtitle)
        public TextView m_tvSubTitle;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    public NosotrosListAdapter(NosotrosListAdapter.RecyclerListener callback) {
        mObjects = new ArrayList<>();
        this.mCallback = callback;

        sdfInput = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.s'Z'");
        sdfOutput = new SimpleDateFormat("dd-MM-yyyy");
    }

    public void add(NosotrosEntity item) {
        mObjects.add(item);
        notifyItemInserted(mObjects.size() - 1);
    }

    public void add(int position, NosotrosEntity item) {
        mObjects.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        mObjects.remove(position);
        notifyItemRemoved(position);
    }

    public void removeAll() {
        int total = mObjects.size();
        for (int i = 0; i < total; i++) {
            mObjects.remove(0);
        }
        notifyItemRangeRemoved(0, total);
    }

    @Override
    public NosotrosListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.list_item_nosotros, parent, false);
        NosotrosListAdapter.ViewHolder vh = new NosotrosListAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(NosotrosListAdapter.ViewHolder holder, final int position) {
        NosotrosEntity data = mObjects.get(position);

        if (data.getTitle()!= null && !data.getTitle().isEmpty()){
            holder.m_tvTitle.setText(data.getTitle());
        }
        if (data.getSubTitle()!= null && !data.getSubTitle().isEmpty()){
             holder.m_tvSubTitle.setText(data.getSubTitle());
        }

        holder.m_CardViewTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCallback != null) {
                    mCallback.onObjectSelected(position, mObjects.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mObjects.size();
    }

    public void setCallback(NosotrosListAdapter.RecyclerListener callback) {
        this.mCallback = callback;
    }
}
