package com.cep.elantra.peru.retrofit.entitys;

import java.io.Serializable;

public class NosotrosEntity implements Serializable {

    private String title;

    private String subTitle;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }
}
