package com.cep.elantra.peru.fragment.NosotroFragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.cep.elantra.peru.R;
import com.cep.elantra.peru.activity.DetalleItemActivity.DetalleItemActivity;
import com.cep.elantra.peru.fragment.NosotroFragment.adapter.NosotrosListAdapter;
import com.cep.elantra.peru.fragment.base.BaseFragment;
import com.cep.elantra.peru.retrofit.entitys.NosotrosEntity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NosotrosFragment extends BaseFragment implements NosotrosListAdapter.RecyclerListener, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = NosotrosFragment.class.getSimpleName();
    private ArrayList<NosotrosEntity> listaNosotros;
    private NosotrosListAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nosotros, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        m_rvRecycler.setLayoutManager(linearLayoutManager);

        mAdapter = new NosotrosListAdapter(this);
        m_rvRecycler.setAdapter(mAdapter);

        loadItemsNosotrosDummy();
        swipe_refresh_layout.setOnRefreshListener(this);
    }

    private void loadItemsNosotrosDummy() {
        listaNosotros = new ArrayList<>();

        NosotrosEntity nosotrosEntity = new NosotrosEntity();
        nosotrosEntity.setTitle("");//.setTitle("Reunión noviembre");
        nosotrosEntity.setSubTitle("");//("Estimados, se les invita a la proxima reunión que será el viernes 15 de noviembre a las 8:30 PM.");
        mAdapter.add(nosotrosEntity);


        NosotrosEntity nosotrosEntity1 = new NosotrosEntity();
        nosotrosEntity1.setTitle("");//.setTitle("Reunión octubre");
        nosotrosEntity1.setSubTitle("");//.setSubTitle("Estimados, se les invita a la proxima reunión que será el viernes 15 de noviembre a las 8:30 PM.");
        mAdapter.add(nosotrosEntity1);

        NosotrosEntity nosotrosEntity2 = new NosotrosEntity();
        nosotrosEntity2.setTitle("");//.setTitle("Reunión setiembre");
        nosotrosEntity2.setSubTitle("");//.setSubTitle("Estimados, se les invita a la proxima reunión que será el viernes 15 de noviembre a las 8:30 PM.");
        mAdapter.add(nosotrosEntity2);

        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onObjectSelected(int position, NosotrosEntity object) {
        Log.e(TAG, "onObjectSelected: " + object.getTitle() != null ? object.getTitle() : "" );
        navigateToDetailActivity();
    }

    private void navigateToDetailActivity(){
        if ( getActivity() == null ){
            return;
        }
        Intent intentDetailActivity = new Intent(getActivity(), DetalleItemActivity.class);
        startActivity(intentDetailActivity);
        getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    @Override
    public void onRefresh() {
        swipe_refresh_layout.setRefreshing(false);
    }


    @NonNull
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipe_refresh_layout;

    @BindView(R.id.rvRecycler)
    RecyclerView m_rvRecycler;


}
